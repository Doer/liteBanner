package com.thebeastshop.banner;

import junit.framework.TestCase;

public class SensitiveFilterTest extends TestCase{
	
	public void test() throws Exception{
		long start = System.currentTimeMillis();
		
		for (int i=0; i<100; i++){
			BannerFilter filter = BannerFilter.DEFAULT;
			
			String sentence = "张开,操黑,插你";
			BannerResp resp = filter.process(sentence);
			System.out.println(resp);
			Thread.sleep(5000);
		}
		System.in.read();
		// 加载默认词典
		long end = System.currentTimeMillis();
		System.out.println("耗时" + (end-start) + "毫秒");
	}
}
